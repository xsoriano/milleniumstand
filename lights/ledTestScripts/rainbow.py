import time

#Load driver for your hardware, visualizer just for example
from bibliopixel.drivers.WS2801  import DriverWS2801
driver = DriverWS2801(num = 32)

#load the LEDStrip class
from bibliopixel.led import *
led = LEDStrip(driver)
import bibliopixel.colors as colors

#load channel test animation
from bibliopixel.animation import Rainbow

anim = Rainbow(led)


try:
	anim.run(fps=30)
except KeyboardInterrupt:
	led.all_off()
	led.update()
