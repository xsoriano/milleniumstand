import time

#Load driver for your hardware, visualizer just for example
from bibliopixel.drivers.WS2801  import DriverWS2801
driver = DriverWS2801(num = 32)

#load the LEDStrip class
from bibliopixel.led import *
led = LEDStrip(driver)
import bibliopixel.colors as colors

#load channel test animation
from bibliopixel.animation import StripChannelTest
anim = StripChannelTest(led)

try:
    for i in range(255):
        for x in range(20):
            #print("Led no. " + str(x))
            if (i%3==0 or i%3==2):
                led.set(x, (i, 255-i, i/2))
            else:
                led.set(x, (i/2,i,255-i ))
            led.update()
            time.sleep(0.5)
        led.all_off()
        led.update()
except KeyboardInterrupt:
    led.all_off()
    led.update()
