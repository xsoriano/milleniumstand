import RPi.GPIO as GPIO
import time
import sys
from weight.hx711 import HX711
import numpy as np
import subprocess
import os
import notification


# scale setup
hx = HX711(5, 6)
hx.set_reading_format("LSB", "MSB")
hx.set_reference_unit(92)
hx.reset()
hx.tare()

# Setup for the paths of where other scripts and music live
HOME_DIR = os.getenv("SYNCHRONIZED_LIGHTS_HOME")
print HOME_DIR
if not HOME_DIR:
    print("Need to setup SYNCHRONIZED_LIGHTS_HOME environment variable, see readme")
    sys.exit()

PROJECT_MUSIC_DIR = HOME_DIR + '/music/millennium'

SCRIPTS_DIR = HOME_DIR + '/bin'

# Map between the weights and the song files
weights_and_music = {0: None, 441: ['imperial.mp3', 'Come to the dark side'],
                     1720: ['lotr.mp3', 'They\'re taking the hobbits to Isengard'],
                     2950: ['starwars.mp3', 'Chewie we\'re home']}


def clean_and_exit():
    print "Cleaning..."
    GPIO.cleanup()
    print "Bye!"
    sys.exit()


def play(song_filename):
    stop_music()
    command = SCRIPTS_DIR + '/play'
    song_path = PROJECT_MUSIC_DIR + '/' + song_filename
    # Use Popen instead of check_call to keep executing the rest of the code
    subprocess.Popen([command, song_path])


def stop_music():
    subprocess.check_call(SCRIPTS_DIR + '/stop_music_and_lights')


def eff_zero(val, threshold=10):
    return abs(val) < threshold


# List of weights is small enough so we can do linear search
def get_closest_existing_weight(weight, threshold=150):
    current_closest = None
    current_delta = float('inf')
    for w in weights_and_music:
        new_delta = abs(weight - w)
        # TODO: Deal with cases on which the weights are too close?
        if new_delta < current_delta:
            current_delta = new_delta
            current_closest = w
        print str(current_closest)
    if eff_zero(current_delta, threshold):
        return current_closest
    else:
        return -1


def run_millennium_stand():
    changing = False
    vals = [0] * 6
    i = 0
    while True:
        try:

            val = hx.get_weight(2)
            print val
            idx = i % len(vals)
            vals[idx] = val
            if i > len(vals):
                if i == len(vals) + 1:
                    ave = np.mean(vals)
                    print 'Ready to weight'
                last10 = vals[idx + 1:] + vals[:idx + 1]
                llm = np.median(last10[:len(last10) / 2])
                lm = np.median(last10[len(last10) / 2:])
                delta = abs(lm - llm)
                print delta, val, i
                if not (eff_zero(delta)):
                    changing = True
                if eff_zero(delta) and changing:
                    print 'New Weight:' + str(val)
                    changing = False
                    closest_weight = get_closest_existing_weight(val)
                    if closest_weight == -1:
                        print 'This object is not in the library'
                        stop_music()
                    elif closest_weight == 0:
                        print 'The object was removed'
                        stop_music()
                    else:
                        print 'Object recognized'
                        notification.send_message(weights_and_music[closest_weight][1])
                        play(weights_and_music[closest_weight][0])

            i += 1
            hx.power_down()
            hx.power_up()
        except (KeyboardInterrupt, SystemExit):
            clean_and_exit()


if __name__ == "__main__":
    run_millennium_stand()


